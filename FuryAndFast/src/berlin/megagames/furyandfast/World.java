package berlin.megagames.furyandfast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

public class World {

	private LinkedList<Actor> actors = new LinkedList<Actor>();

	private final int width;
	private final int height;

	private WorldPanel scene;

	private String name;

	public World(int width, int height, Stage primaryStage) {
		this.width = width;
		this.height = height;
		scene = new WorldPanel(primaryStage, this);
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Actor> getActorList() {
		return actors;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean isKeyDown(String string) {
		KeyCode k = KeyCode.getKeyCode(string);
		return (k != null) && scene.isKeyPressed(k);
	}

	public void addObject(Actor actor, int x, int y) {
		actor.addToWorld(x, y, this);
		actors.add(actor);
		scene.addActorNode(actor.iv);
	}

	public void removeObject(Actor actor) {
		if (actor == null)
			return;

		actors.remove(actor);
		scene.removeActorNode(actor.iv);
	}

	public List<Actor> getIntersectingObjects(Actor actor, Class<Actor> cls) {

		ArrayList<Actor> listCollisions = new ArrayList<>();

		double maxdist = 20 * 20;

		for (int i = 0; i < actors.size(); i++) {

			if (actors.get(i).getClass() != cls)
				continue;

			if (actors.get(i) == actor)
				continue;

			// System.out.println("Class matches " + cls.getName());
			double dist = (actors.get(i).getPosX() - actor.getPosX()) * (actors.get(i).getPosX() - actor.getPosX())
					    + (actors.get(i).getPosY() - actor.getPosY()) * (actors.get(i).getPosY() - actor.getPosY());
			if (dist < maxdist) {
				listCollisions.add(actors.get(i));
			}
		}
		return listCollisions;
	}

	public void showText(String text) {
		scene.showText(text);
	}

	public void act() {
	}

	public String getName() {
		return this.name;
	}

	public void delay(int delay) {
		scene.delay(delay);
	}

	public void exit() {
		scene.exit();
	}
}
