package berlin.megagames.furyandfast;

import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class Actor {

	public double getPosX() {
		return posX;
	}

	public double getPosY() {
		return posY;
	}

	private double posX;
	private double posY;
	private int degree;

	private World world;

	/** Current display node */
	public ImageView iv;

	public Actor(Image image) {
		iv = new ImageView();
		iv.setImage(image);
	}

	protected void setPosX(double posX) {
		if (posX < 0 || posX > world.getWidth())
			return;
		this.posX = posX;
		iv.setX(posX);
	}

	protected void setPosY(double posY) {
		if (posY < 0 || posY > world.getHeight())
			return;
		this.posY = posY;
		iv.setY(posY);
	}

	protected void delay(int delay) {
		world.delay(delay);
	}

	@SuppressWarnings("rawtypes")
	public List<Actor> getIntersectingObjects(Class cls) {
		List<Actor> l = world.getIntersectingObjects(this, cls);
		l.remove(this);
		return l;
	}

	@SuppressWarnings("rawtypes")
	public Actor getOneIntersectingObject(Class cls) {
		List<Actor> a = getIntersectingObjects(cls);
		if (a.size() > 0)
			return getIntersectingObjects(cls).get(0);
		else
			return null;

	}

	protected void turn(int degree) {
		this.degree = (this.degree + degree) % 360;
		iv.setRotate(this.degree);
	}

	protected void move(double distance) {

		double radians = Math.toRadians(this.degree);

		// We round to the nearest integer, to allow moving one unit at an angle
		// to actually move.
		double dx = Math.round(Math.cos(radians) * distance * 10.0) / 10.0;
		double dy = Math.round(Math.sin(radians) * distance * 10.0) / 10.0;
		setPosX(this.posX + dx);
		setPosY(this.posY + dy);
	}

	protected boolean isKeyDown(String key) {
		return world.isKeyDown(key);
	}

	public void addToWorld(int x, int y, World world) {
		this.world = world;
		this.setPosX(x);
		this.setPosY(y);
	}

	public World getWorld() {
		return world;
	}

	public void act() {

	}
}
