package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	private String typ;
	private String antrieb;
	private int maxLadekapazitaet;
	private double posX;
	private double posY;
	private int Winkel;
	
	
	// Methoden

	/**
	 * @return the type
	 */
	public String getTyp() {
		return typ;
	}


	/**
	 * @param type the type to set
	 */
	public void setTyp(String typ) {
		this.typ = typ;
	}


	/**
	 * @return the antrieb
	 */
	public String getAntrieb() {
		return antrieb;
	}


	/**
	 * @param antrieb the antrieb to set
	 */
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	

	/**
	 * @return the maxLadekapazitaet
	 */
	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
	}


	/**
	 * @param maxLadekapazitaet the maxLadekapazitaet to set
	 */
	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}


	/**
	 * @return the posX
	 */
	public double getPosX() {
		return posX;
	}


	/**
	 * @param posX the posX to set
	 */
	public void setPosX(double posX) {
		this.posX = posX;
	}


	/**
	 * @return the posY
	 */
	public double getPosY() {
		return posY;
	}


	/**
	 * @param posY the posY to set
	 */
	public void setPosY(double posY) {
		this.posY = posY;
	}


	/**
	 * @return the winkel
	 */
	public int getWinkel() {
		return Winkel;
	}


	/**
	 * @param winkel the winkel to set
	 */
	public void setWinkel(int winkel) {
		Winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
