package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	private String typ;
	private int masse;
	private double posX;
	private double posY;
	

	// Methoden
	/**
	 * @return the typ
	 */
	public String getTyp() {
		return typ;
	}


	/**
	 * @param typ the typ to set
	 */
	public void setTyp(String typ) {
		this.typ = typ;
	}


	/**
	 * @return the masse
	 */
	public int getMasse() {
		return masse;
	}


	/**
	 * @param masse the masse to set
	 */
	public void setMasse(int masse) {
		this.masse = masse;
	}


	/**
	 * @return the posX
	 */
	public double getPosX() {
		return posX;
	}


	/**
	 * @param posX the posX to set
	 */
	public void setPosX(double posX) {
		this.posX = posX;
	}


	/**
	 * @return the posY
	 */
	public double getPosY() {
		return posY;
	}


	/**
	 * @param posY the posY to set
	 */
	public void setPosY(double posY) {
		this.posY = posY;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}

}