/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */

	  int count;
	  
    /* 2. Weisen Sie dem Zaehler den Werte 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/

	  count = 25;
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */

	  char menuItem;
	  
	  
    /* 4. Weisen Sie dem Buchstaben den Werte 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  
	  menuItem = 'C';
	  
	  System.out.printf("***Es wurde %c als Menueitem gewaehlt\n", menuItem);

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwert
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  
	  double astroCalc;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie diesie auf dem Bildschirm aus.*/
	  
	  astroCalc = 299792458.0; //cm/s
	  System.out.printf("***Die lichtgeschwindigkeit wurde auf %.1f m/s festgelegt\n", astroCalc);


    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  
	  byte vereinsMitglieder = 7;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  
	  System.out.printf("***Die aktuellen Mitglieder des Vereins betragen %d Kuchen\n", vereinsMitglieder);


    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignetes Attribut und geben Sie es auf
          dem Bildschirm aus.*/
	  
	  double elementarLadung = 1.602177 * Math.pow(10, -19);
	  System.out.printf("***Die elementarladung wurde auf %f m/s festgelegt\n", elementarLadung);

	  
	  
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  Boolean zahlungErfolgt;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variablen den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/

	  zahlungErfolgt = true;
	  System.out.printf("***Die zahlung ist %serfolgt\n", !zahlungErfolgt ? "nicht " : "");
	  
  }//main
}// Variablen