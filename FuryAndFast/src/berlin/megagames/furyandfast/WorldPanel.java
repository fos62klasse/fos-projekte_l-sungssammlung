package berlin.megagames.furyandfast;

import java.util.HashMap;
import java.util.Map;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class WorldPanel {

	private Label lblInfo = new Label("");

	private BorderPane pnlMain = new BorderPane();
	private Pane pnlCenter = new Pane();
	private FlowPane pnlSouth = new FlowPane();
	
	@SuppressWarnings("unused")
	private World world;
	
	private Scene myScene;

	private Map<KeyCode, Boolean> keys = new HashMap<KeyCode, Boolean>();

	private long delay;
	
	public WorldPanel(Stage primaryStage, World world) {


		// Attach world
		this.world = world;
		
		// Setze Schriftart
		this.lblInfo.setFont(Font.font(null, FontWeight.BOLD, 16));
		pnlSouth.getChildren().add(lblInfo);

		pnlCenter.setPrefWidth(800);
		pnlCenter.setPrefHeight(600);
		pnlCenter.setStyle("-fx-background-image:url('rivets.jpg');-fx-background-repeat:repeat");

		// Fuege Panes hinzu
		pnlMain.setCenter(pnlCenter);
		pnlMain.setBottom(pnlSouth);
		
		final long startNanoTime = System.nanoTime();

		new AnimationTimer() {
			public void handle(long currentNanoTime) {
				@SuppressWarnings("unused")
				// Time passed
				double t = (currentNanoTime - startNanoTime) / 1000000000.0;
				
				// Check for delay
				if (delay - currentNanoTime> 0)
				{
					return;
				}
				
				// Logic
				world.act();
				// Actors act 
				for (int i = 0; i < world.getActorList().size(); i++)
				{
					 world.getActorList().get(i).act();
				}
									
			}
		}.start();

		// Lade Scene und Stage
		myScene = new Scene(pnlMain);
		
		// Wire up properties to key events:
		myScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent ke) {
		    	keys.put(ke.getCode(), true);
		    }
		});

		myScene.setOnKeyReleased(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent ke) {
		    	keys.put(ke.getCode(), false);
		    }
		});

		
		primaryStage.setTitle(world.getName());
		primaryStage.setScene(myScene);
		primaryStage.sizeToScene();
		primaryStage.show();
		
	}
	
	public void addActorNode(Node e)
	{
		pnlMain.getChildren().add(e);
	}
	
	public void removeActorNode(Node e)
	{
		pnlMain.getChildren().remove(e);
	}
	
	public boolean isKeyPressed(KeyCode k)
	{
		return keys.containsKey(k) && keys.get(k);		
	}

	public void showText(String text) {
		lblInfo.setText(text);		
	}

	public void delay(int delay) {
		this.delay = System.nanoTime() + delay * 1000000;		
	}

	public void exit() {
		((Stage) (this.myScene.getWindow())).close();		
	}
}
