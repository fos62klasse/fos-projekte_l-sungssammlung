
public class AngestellterTest {

	public static void main(String[] args) {

		Angestellter ang1 = new Angestellter();
		Angestellter ang2 = new Angestellter("Petersen", 16, 2500);
		
		ang1.setName("Meier");
		ang1.setErfahrung(10);
		
		System.out.println("Name: " + ang1.getName());
		System.out.println("Gehalt: "+ang1.berechneGehalt());
		System.out.println(ang2.toString());
		
		if (ang1.hatMehrErfahrungAls(ang2)) {
			System.out.println(ang1.getName()+
								" ist erfahrener als"+ ang2.getName());
		} else {
			System.out.println(ang2.getName()+ " ist erfahrener als "+ ang1.getName());
		}
		
	}

}
