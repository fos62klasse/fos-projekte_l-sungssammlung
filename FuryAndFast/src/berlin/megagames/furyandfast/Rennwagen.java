package berlin.megagames.furyandfast;

import javafx.scene.image.Image;

/**
 * Rennwagen, lässt sich durch die Welt steuern
 * 
 * @author  MegaGames Berlin 
 * @version 0.1alpha
 */
public class Rennwagen extends Actor
{

    private String name;
    private String farbe;
    private int vmax;

    // aktuelle Geschwindigkeit
    private int vaktuell;

    /**
     * Standardkonstruktor
     */
    public Rennwagen()
    {
        // Fahrzeug steht normalerweise am Anfang - Geschwindigkeit ist 0
        // --> rufe Konstruktor mit vstart = 0 auf
        this(0,0,0);
    }

    /**
     * Konstruktor mit Startgeschwindigkeit
     * @param vstart Startgeschwindigkeit des Rennwages
     */
    public Rennwagen(int posX, int posY, int vstart)
    {
    	super(new Image("racingcar.png"));
        this.vaktuell = vstart;
    }

    // Verwaltungsmethoden 
    /**
     * Setzt den Namen
     * 
     *  @param _name Name des Rennwages
     */  
    public void setName(String _name)
    {
        this.name = _name;
    }

    /**
     * Gibt die den Namen des Rennwages zurück
     * 
     *  @return Name des Rennwages
     */  
    public String getName()
    {
        return this.name;
    }

    /**
     * Setzt die Farbe
     * 
     *  @param _farbe Farbe des Rennwages
     */  
    public void setFarbe(String _farbe)
    {
        this.farbe = _farbe;
    }

    /**
     * Gibt die Farbe zurück
     * 
     *  @return Farbe des Rennwages
     */  
    public String getFarbe()
    {
        return this.farbe;
    }

    /**
     * Setzt die Maximalgeschwindigkeit
     * 
     *  @param _vmax Maximalgeschwindigkeit des Rennwages
     */  
    public void setVmax(int _vmax)
    {
        // Nur positive vmax möglich
        this.vmax = Math.abs(_vmax);
    }

    /**
     * Gibt die Maximalgeschwindigkeit zurück
     * 
     *  @return Maximalgeschwindigkeit des Rennwages
     */  
    public int getVmax()
    {
        return this.vmax;
    }

    /**
     * Gibt die aktuelle Geschwindigkeit zurück
     * 
     *  @return aktuelle Geschwindigkeit des Rennwages
     */    
    public int getVaktuell()
    {
        return vaktuell;
    }

    /**
     *  lenkt den Wagen nach links
     */
    public void lenkeLinks()
    {
        turn(-5);
    }

    /**
     *  lenkt den Wagen nach rechts
     */
    public void lenkeRechts()
    {
        turn(+5);
    }

    /**
     *  beschleunigt den Wagen
     */
    public void beschleunige()
    {
        vaktuell = Math.min(vmax, (vaktuell + 1));
    }

    /**
     *  bremst den Wagen ab
     */
    public void bremseAb()
    {
        vaktuell = Math.max(-vmax, (vaktuell - 1));            
    }

    // Game Play
    /**
     * Rennwagen beschleunigen/bremsen und lenken
     */
    public void act() 
    {
        // Verarbeite Beschleunigung   
        if (isKeyDown("Up"))
        {           
            beschleunige();
        }
        else if(isKeyDown("Down"))
        {
            bremseAb();
        }
        else
        {            
            if (vaktuell > 1)
            {
                bremseAb();
            }
            else if (vaktuell < -1)
            {
                beschleunige();   
            }
            else
                vaktuell = 0;

        }                           

        // Fahre mit aktueller Geschwindigkeit - 20 km/h pro Pixel
        move(vaktuell / 20.0);      

        // Lenke links
        if (isKeyDown("Left"))
            lenkeLinks();

        // Lenke rechts   
        if (isKeyDown("Right"))
            lenkeRechts();

        // Zeige Geschwindigkeit an
        this.getWorld().showText(String.format("Geschwindigkeit: %d km/h", this.vaktuell));

    }    
}
