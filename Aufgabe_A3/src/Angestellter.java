
public class Angestellter {

	private String name;
	private int erfahrung;

	public Angestellter() {
	}

	public Angestellter(String name, int erfahrung, double gehalt) {
		this.name = name;
		this.erfahrung = erfahrung;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getErfahrung() {
		return erfahrung;
	}

	public void setErfahrung(int erfahrung) {
		this.erfahrung = erfahrung;
	}

	public boolean hatMehrErfahrungAls(Angestellter ang) {
		return erfahrung > ang.getErfahrung();
	}

	public double berechneGehalt() {

		double gehalt = 2500;
		if (this.getErfahrung() <= 5) {

			gehalt = gehalt + this.getErfahrung() * 100;
		
		} else if (this.getErfahrung() > 5 && this.getErfahrung() <= 15) {

			gehalt = gehalt + this.getErfahrung() * 150;
		
		} else {

			gehalt= gehalt+ this.getErfahrung() * 200;
		
		}
		
		return gehalt;
	}

	@Override
	public String toString() {
		return "Angestellter [name=" + name + ", erfahrung=" + erfahrung +"]";
	}

}
